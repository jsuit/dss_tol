FROM tolproject/tol-prod-xenial:3.4
RUN /usr/bin/apt-get update -yqq \
 && /usr/bin/apt-get upgrade --no-install-recommends -yqq \
 && /usr/bin/apt-get install --no-install-recommends -yqq locales \
 && /usr/bin/apt-get install --no-install-recommends -yqq acl curl git sudo \
    libexpat1 zip unzip nginx default-jre-headless python2.7 libpython2.7 \
    libfreetype6 libgfortran3 libgomp1 \
    libcurl4-openssl-dev libssl-dev libxml2-dev libzmq3-dev \
    ssh \
 && /usr/sbin/groupadd -r dataiku \
 && /usr/sbin/useradd -r -m -s /bin/bash -g dataiku dataiku \
 && /bin/echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
 && /usr/sbin/locale-gen \
 && echo "dataiku ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/dataiku
ENV DSS_VERSION=5.0.1
ENV DSS_DATADIR=/home/dataiku/dss
ENV DSS_PORT=10000
ENV DSS_INSTALLDIR=/home/dataiku/dataiku-dss-$DSS_VERSION
ENV R_HOME=/usr/lib/R
USER dataiku
ADD --chown=dataiku:dataiku dataiku-dss-${DSS_VERSION}.tar.gz run.sh /home/dataiku/
ADD cleaner.sh /tmp/
RUN  ssh-keygen -t rsa -C "git_dss" -b 4096 -N '' -f /home/dataiku/.ssh/id_rsa -q \
 && cat /home/dataiku/.ssh/id_rsa.pub \
 && mkdir $DSS_DATADIR \
 && ${DSS_INSTALLDIR}/installer.sh -d $DSS_DATADIR -p $DSS_PORT \
 && ${DSS_DATADIR}/bin/dssadmin install-R-integration \
 && sudo /tmp/cleaner.sh
WORKDIR /home/dataiku
EXPOSE $DSS_PORT
VOLUME $DSS_DATADIR
ENTRYPOINT ["/home/dataiku/run.sh"]
